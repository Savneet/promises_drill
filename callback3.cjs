/* 
	Problem 3: Write a function that will return all cards that belong to a particular list based on the listID that is passed to it 
    from the given data in cards.json. Then pass control back to the code that called it by using a callback function.
*/

const cards = require("./cards.json");

function returnCards(listID) {
  return new Promise((resolve, reject) => {
    setTimeout(function () {
      let listIDs = Object.keys(cards);
      let givenListId = listIDs.find((element) => element == listID);
      if (givenListId) {
        let cardsOfGivenId = cards[givenListId];
        resolve(cardsOfGivenId);
      } else {
        reject("No lists of given board id");
      }
    }, 2000);
  });
}

module.exports = returnCards;
