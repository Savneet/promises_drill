/* 
	Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

const getBoardInfo = require("./callback1.cjs");
const getListOfBoardId = require("./callback2.cjs");
const getCardOfListId = require("./callback3.cjs");
const cards = require("./cards.json");

function usePreviousFunc() {
  setTimeout(function () {
    getBoardInfo("mcu453ed")
      .then((info) => console.log(info))
      .catch((error) => console.log(error));

    getListOfBoardId("mcu453ed")
      .then((info) => console.log(info))
      .catch((error) => console.log(error));

    const getAllListIds = Object.keys(cards);

    getAllListIds.forEach((listId) => {
      getCardOfListId(listId)
        .then((info) => console.log(info))
        .catch((error) => console.log(error));
    });
  }, 2000);
}

module.exports = usePreviousFunc;

