/*Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given 
list of boards in boards.json and then pass control back to the code that called it by using a callback function.*/

const boards = require("./boards.json");

function boardInfo(boardID) {
  return new Promise((resolve, reject) => {
    setTimeout(function () {
      let info = boards.find((element) => {
        return element.id === boardID;
      });
      if (info) {
        resolve(info);
      } else {
        reject("No Board with that board id");
      }
    }, 2000);
  });
}

module.exports = boardInfo;
