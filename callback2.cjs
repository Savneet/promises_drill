/*Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it 
from the given data in lists.json. Then pass control back to the code that called it by using a callback function.*/
const lists = require("./lists_1.json");

function returnList(boardID) {
  return new Promise((resolve, reject) => {
    setTimeout(function () {
      let boardIDs = Object.keys(lists);
      let givenBoardId = boardIDs.find((element) => element == boardID);
      if (givenBoardId) {
        let listsOfGivenId = lists[givenBoardId];
        resolve(listsOfGivenId);
      } else {
        reject("No lists of given board id");
      }
    }, 2000);
  });
}

module.exports = returnList;
